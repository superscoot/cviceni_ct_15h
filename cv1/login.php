<!DOCTYPE html>

<html>
<head>	
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<main>
		<nav>
			<ul>
				<li><a href="#">Menu1</a></li>
				<li><a href="#">Menu2</a></li>
				<li><a href="#">Menu3</a></li>
				<li><a href="login.php">Users</a></li>
			</ul>
		</nav>

		<section>
			<form>
				<label>Username 
					<input type="text" name="username" required>
				</label>
				<br>
				<label>Password 
					<input type="password" name="passwd" required>
				</label>
				<br>
				<label>Planet
					<select name="planet"> 
						<option value="Earth">Earth</option>
						<option value="Betelgeuze">Betelgeuze</option>
						<option value="Proxima B">Proxima B</option>
					</select>
				</label>
				<br>
				<input type="submit" value="Send">
			</form>
		</section>

		<?php 
			if (!empty($_GET['username'])) {
				print "<h1>Welcome " . htmlspecialchars($_GET['username']) . "<h1>";
			}
		?>
	</main>
</body>
</html>
